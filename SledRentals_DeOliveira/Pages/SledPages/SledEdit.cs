﻿using Microsoft.AspNetCore.Components;
using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Pages.SledPages
{
    public partial class SledEdit
    {
        [Parameter]
        public string? SledID { get; set; }
        private Sled Sled = new Sled();
        private List<SledCategory> SledCategories;
        protected async Task OnValidSubmission()
        {         
            await uow.RentalRepo.CreateOrUpdateSledAsync(Sled);
            NavManager.NavigateTo("/sledsoverview");
        }

        protected async override Task OnInitializedAsync()
        {
            if (SledID != null)
            {
                var sled = await uow.RentalRepo.GetSledAsync(int.Parse(SledID));
                if (sled != null) Sled = sled;
            }
            SledCategories = await uow.RentalRepo.GetSledCategoriesAsync();        
        }
    }
}
