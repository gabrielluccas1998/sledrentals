﻿using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Pages.SledPages
{
    public partial class SledOverview
    {
        private List<Sled> sleds;
        protected override async Task OnInitializedAsync()
        {
            sleds = await uow.RentalRepo.GetSledsAsync();
        }

        private async Task DeleteSledAsync(int id)
        {
            await uow.RentalRepo.DeleteSledAsync(id);
            sleds = await uow.RentalRepo.GetSledsAsync();
            StateHasChanged();
        }
    }
}
