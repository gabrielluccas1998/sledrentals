﻿using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Pages.RentalPages
{
    public partial class RentalOverview
    {
        private List<Rental> rentals;
        protected override async Task OnInitializedAsync()
        {
            rentals = await uow.RentalRepo.GetRentalsAsync();
        }

        private async Task DeleteRentalAsync(int id)
        {
            await uow.RentalRepo.DeleteRentalAsync(id);
            rentals = await uow.RentalRepo.GetRentalsAsync();
            StateHasChanged();
        }
    }
}
