﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Pages.RentalPages
{
    public partial class RentalEdit
    {
        [Parameter]
        public string? RentalID { get; set; }
        private Rental Rental = new Rental();
        private List<Sled> Sleds;

        private readonly UserManager<ApplicationUser> _userManager;

        protected async Task OnValidSubmission()
        {
            var User = await _userManager.GetUserAsync(httpContextAcessor.HttpContext.User);
            Rental.UserID = User.Id;
            await uow.RentalRepo.CreateOrUpdateRentalAsync(Rental);
            NavManager.NavigateTo("/reservation");
        }

        protected async override Task OnInitializedAsync()
        {
            if (RentalID != null)
            {
                var rental = await uow.RentalRepo.GetRentalAsync(int.Parse(RentalID));
                if (rental != null) Rental = rental;
            }
            Sleds = await uow.RentalRepo.GetSledsAsync();
        }
    }
}
