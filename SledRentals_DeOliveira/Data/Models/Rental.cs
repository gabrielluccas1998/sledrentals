﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SledRentals_DeOliveira.Data.Models
{
    public class Rental
    {
        [Key]
        public int RentalID { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public DateTime ValidFrom { get; set; }
        [Required]
        public DateTime ValidTo { get; set; }
        [ForeignKey(nameof(Sled)), Required]
        public int SledID { get; set; }
        [ForeignKey(nameof(ApplicationUser)), StringLength(450)]
        public string? UserID { get; set; }
        public virtual Sled? Sled { get; set; }
        public virtual ApplicationUser? User { get; set; }
    }
}
