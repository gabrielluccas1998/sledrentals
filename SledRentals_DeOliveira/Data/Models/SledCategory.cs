﻿using System.ComponentModel.DataAnnotations;

namespace SledRentals_DeOliveira.Data.Models
{
    public class SledCategory
    {
        [Key]
        public int SledCategoryID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [StringLength(250)]
        public string? Description { get; set; }
        public virtual ICollection<Rental>? Sleds { get; set; }
    }
}
