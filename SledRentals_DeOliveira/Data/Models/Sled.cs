﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SledRentals_DeOliveira.Data.Models
{
    public class Sled
    {
        [Key]
        public int SledID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required]
        public DateTime PurchaseDate { get; set; }
        [ForeignKey(nameof(SledCategory)), Required]
        public int SledCategoryID { get; set; }
        public virtual SledCategory? SledCategory { get; set; }
    }
}
