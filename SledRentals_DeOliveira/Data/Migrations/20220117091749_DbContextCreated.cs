﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SledRentals_DeOliveira.Data.Migrations
{
    public partial class DbContextCreated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SledCategories",
                columns: table => new
                {
                    SledCategoryID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SledCategories", x => x.SledCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "Sleds",
                columns: table => new
                {
                    SledID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PurchaseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SledCategoryID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sleds", x => x.SledID);
                    table.ForeignKey(
                        name: "FK_Sleds_SledCategories_SledCategoryID",
                        column: x => x.SledCategoryID,
                        principalTable: "SledCategories",
                        principalColumn: "SledCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rentals",
                columns: table => new
                {
                    RentalID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ValidFrom = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ValidTo = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SledID = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rentals", x => x.RentalID);
                    table.ForeignKey(
                        name: "FK_Rentals_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rentals_Sleds_SledID",
                        column: x => x.SledID,
                        principalTable: "Sleds",
                        principalColumn: "SledID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "1", "30f2e509-bd18-446c-a73f-e5c2a07a3e44", "Admin", null },
                    { "2", "628a4018-a2cc-4ef9-852c-7afc6384b26e", "Customer", null }
                });

            migrationBuilder.InsertData(
                table: "SledCategories",
                columns: new[] { "SledCategoryID", "Description", "Title" },
                values: new object[,]
                {
                    { 1, "To go fast as the wind!", "Rennschlitten" },
                    { 2, "Nur für eine Person!", "Einsitzer" },
                    { 3, "Für 2!", "Zweisitzer" }
                });

            migrationBuilder.InsertData(
                table: "Sleds",
                columns: new[] { "SledID", "PurchaseDate", "SledCategoryID", "Title" },
                values: new object[] { 1, new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4661), 1, "LION, Hörnerrodel mit Gurtsitz" });

            migrationBuilder.InsertData(
                table: "Sleds",
                columns: new[] { "SledID", "PurchaseDate", "SledCategoryID", "Title" },
                values: new object[] { 2, new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4694), 2, "Kathrein, Flexi-Kinderlehne" });

            migrationBuilder.InsertData(
                table: "Sleds",
                columns: new[] { "SledID", "PurchaseDate", "SledCategoryID", "Title" },
                values: new object[] { 3, new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4696), 3, "Hamax, Sno Action Lenkbob" });

            migrationBuilder.CreateIndex(
                name: "IX_Rentals_SledID",
                table: "Rentals",
                column: "SledID");

            migrationBuilder.CreateIndex(
                name: "IX_Rentals_UserID",
                table: "Rentals",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Sleds_SledCategoryID",
                table: "Sleds",
                column: "SledCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Rentals");

            migrationBuilder.DropTable(
                name: "Sleds");

            migrationBuilder.DropTable(
                name: "SledCategories");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");
        }
    }
}
