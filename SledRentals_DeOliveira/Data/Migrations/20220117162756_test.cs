﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SledRentals_DeOliveira.Data.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SledCategoryID",
                table: "Rentals",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "9f0fc1db-f588-40bd-9070-4fa3202b9649");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "bfc7b17a-233c-480b-8620-17a14e9a15d7");

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 1,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 17, 27, 56, 350, DateTimeKind.Local).AddTicks(4838));

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 2,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 17, 27, 56, 350, DateTimeKind.Local).AddTicks(4866));

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 3,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 17, 27, 56, 350, DateTimeKind.Local).AddTicks(4868));

            migrationBuilder.CreateIndex(
                name: "IX_Rentals_SledCategoryID",
                table: "Rentals",
                column: "SledCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rentals_SledCategories_SledCategoryID",
                table: "Rentals",
                column: "SledCategoryID",
                principalTable: "SledCategories",
                principalColumn: "SledCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rentals_SledCategories_SledCategoryID",
                table: "Rentals");

            migrationBuilder.DropIndex(
                name: "IX_Rentals_SledCategoryID",
                table: "Rentals");

            migrationBuilder.DropColumn(
                name: "SledCategoryID",
                table: "Rentals");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "30f2e509-bd18-446c-a73f-e5c2a07a3e44");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "628a4018-a2cc-4ef9-852c-7afc6384b26e");

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 1,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4661));

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 2,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4694));

            migrationBuilder.UpdateData(
                table: "Sleds",
                keyColumn: "SledID",
                keyValue: 3,
                column: "PurchaseDate",
                value: new DateTime(2022, 1, 17, 10, 17, 49, 190, DateTimeKind.Local).AddTicks(4696));
        }
    }
}
