﻿using SledRentals_DeOliveira.Data.Repositories;

namespace SledRentals_DeOliveira.Data
{
    public class UnitOfWork:IUnitOfWork
    {
        private ApplicationDbContext dbContext;
        private RentalRepo rentalRepo;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public RentalRepo RentalRepo
        {
            get
            {
                if (rentalRepo == null) rentalRepo = new RentalRepo(dbContext);
                return rentalRepo;
            }
        }
    }
}
