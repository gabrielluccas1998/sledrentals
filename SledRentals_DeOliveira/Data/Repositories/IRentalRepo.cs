﻿using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Data.Repositories
{
    public interface IRentalRepo
    {
        Task CreateOrUpdateSledAsync(Sled sled);
        Task CreateOrUpdateRentalAsync(Rental rental);
        Task DeleteRentalAsync(int id);
        Task DeleteSledAsync(int id);
        Task<Rental> GetRentalAsync(int id);
        Task<List<Rental>> GetRentalsAsync();
        Task<Sled> GetSledAsync(int id);
        Task<List<Sled>> GetSledsAsync();
        Task<List<SledCategory>> GetSledCategoriesAsync();
    }
}
