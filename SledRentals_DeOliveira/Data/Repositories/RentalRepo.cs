﻿using Microsoft.EntityFrameworkCore;
using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Data.Repositories
{
    public class RentalRepo:IRentalRepo
    {
        private ApplicationDbContext dbContext;

        //Constructor, when initialized by the UOW get the current dbContext
        public RentalRepo(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        //Sled Repository
        #region Sled CRUD
        //Get a Sled by its ID from the DB
        public async Task<Sled> GetSledAsync(int id)
        {
            return await dbContext.Sleds.FirstOrDefaultAsync(x => x.SledID == id);
        }

        //Get all Sleds from the DB
        public async Task<List<Sled>> GetSledsAsync()
        {
            return await dbContext.Sleds.Include("SledCategory").ToListAsync();
        }

        //Remove a Sled by its ID from the DB
        public async Task DeleteSledAsync(int id)
        {
            var sled = await GetSledAsync(id); // Check, whether the current Sled already exists in the DB
            if (sled != null) dbContext.Sleds.Remove(sled);
            await CommitAsync();
        }
        
        //Create a new Sled or update an existing one
        public async Task CreateOrUpdateSledAsync(Sled sled)
        {
            var oSled = await GetSledAsync(sled.SledID); // Check, whether the current Sled already exists in the DB
            if (oSled == null)
            {
                dbContext.Sleds.Add(sled);
            }
            else
            {
                dbContext.Sleds.Update(sled);
            }
            await CommitAsync();
        }
        #endregion

        //SledCategory Repository
        #region SledCategory CRUD

        //Get all Sleds from the DB
        public async Task<List<SledCategory>> GetSledCategoriesAsync()
        {
            return await dbContext.SledCategories.ToListAsync();
        }
        #endregion

        //Rental Repository
        #region Rental CRUD
        //Get a Rental by its ID from the DB
        public async Task<Rental> GetRentalAsync(int id)
        {
            return await dbContext.Rentals.FirstOrDefaultAsync(x => x.SledID == id);
        }

        //Get all Rentals from the DB
        public async Task<List<Rental>> GetRentalsAsync()
        {
            return await dbContext.Rentals.ToListAsync();
        }

        //Remove a Rental by its ID from the DB
        public async Task DeleteRentalAsync(int id)
        {
            var rental = await GetRentalAsync(id); // Check, whether the current Rental already exists in the DB
            if (rental != null) dbContext.Rentals.Remove(rental);
            await CommitAsync();
        }

        //Create a new Rental or update an existing one
        public async Task CreateOrUpdateRentalAsync(Rental rental)
        {
            var oRental = await GetSledAsync(rental.SledID); // Check, whether the current Rental already exists in the DB
            if (oRental == null)
            {
                dbContext.Rentals.Add(rental);
            }
            else
            {
                dbContext.Rentals.Update(rental);
            }
            await CommitAsync();
        }
        #endregion

        //Utilities Methods
        #region Utilities
        //Save the changes asynchronous on the DB
        public async Task CommitAsync()
        {
            await dbContext.SaveChangesAsync();
        }
        #endregion
    }
}
