﻿using SledRentals_DeOliveira.Data.Repositories;

namespace SledRentals_DeOliveira.Data
{
    public interface IUnitOfWork
    {
        RentalRepo RentalRepo { get; }
    }
}
