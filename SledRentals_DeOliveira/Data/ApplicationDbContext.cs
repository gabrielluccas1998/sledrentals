﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SledRentals_DeOliveira.Data.Models;

namespace SledRentals_DeOliveira.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<SledCategory> SledCategories { get; set; }
        public DbSet<Sled> Sleds { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole { Id = "1", Name = "Admin" },
                new IdentityRole { Id = "2", Name = "Customer" }
                );
            builder.Entity<SledCategory>().HasData(
                new SledCategory { SledCategoryID = 1, Title = "Rennschlitten", Description = "To go fast as the wind!" },
                new SledCategory { SledCategoryID = 2, Title = "Einsitzer", Description = "Nur für eine Person!" },
                new SledCategory { SledCategoryID = 3, Title = "Zweisitzer", Description = "Für 2!" }
                );
            builder.Entity<Sled>().HasData(
                new Sled { SledID = 1, Title = "LION, Hörnerrodel mit Gurtsitz", PurchaseDate = DateTime.Now, SledCategoryID = 1 },
                new Sled { SledID = 2, Title = "Kathrein, Flexi-Kinderlehne", PurchaseDate = DateTime.Now, SledCategoryID = 2 },
                new Sled { SledID = 3, Title = "Hamax, Sno Action Lenkbob", PurchaseDate = DateTime.Now, SledCategoryID = 3 }
                );
        }
    }
}